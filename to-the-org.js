function sendToOrg(url) {
    console.log("Sending to orgmode");
    var loc = "org-protocol:///capture?url="+encodeURIComponent(url);
    location.href = loc;
};


function closeTab(tabId) {
    var removing = browser.tabs.remove(tabId);
    removing.then();
};

function onGet(tabInfo) {
    console.log(tabInfo);
    sendToOrg(tabInfo[0].url);
    closeTab(tabInfo[0].id);
};

function onError(error) {
    console.log(error);
};


function handleClick() {
    var gettingActiveTab = browser.tabs.query({active: true, currentWindow: true})
    gettingActiveTab.then(onGet, onError);
};



browser.browserAction.onClicked.addListener(handleClick);
